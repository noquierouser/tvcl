<?php
// Register Custom Taxonomy
function tvcl_radio_category() {
  $labels = array(
    'name'                       => _x('Categories', 'Taxonomy General Name', 'tvcl'),
    'singular_name'              => _x('Category', 'Taxonomy Singular Name', 'tvcl'),
    'menu_name'                  => __('Category', 'tvcl'),
    'all_items'                  => __('All categories', 'tvcl'),
    'parent_item'                => __('Parent category', 'tvcl'),
    'parent_item_colon'          => __('Parent category:', 'tvcl'),
    'new_item_name'              => __('New category', 'tvcl'),
    'add_new_item'               => __('Add new category', 'tvcl'),
    'edit_item'                  => __('Edit category', 'tvcl'),
    'update_item'                => __('Update category', 'tvcl'),
    'view_item'                  => __('View category', 'tvcl'),
    'separate_items_with_commas' => __('Separate categories with commas', 'tvcl'),
    'add_or_remove_items'        => __('Add or remove categories', 'tvcl'),
    'choose_from_most_used'      => __('Choose from the most used', 'tvcl'),
    'popular_items'              => __('Popular categories', 'tvcl'),
    'search_items'               => __('Search categories', 'tvcl'),
    'not_found'                  => __('Category not found', 'tvcl'),
    'no_terms'                   => __('No categories', 'tvcl'),
    'items_list'                 => __('Category list', 'tvcl'),
    'items_list_navigation'      => __('Category list navigation', 'tvcl'),
  );

  $args = array(
    'labels'                     => $labels,
    'hierarchical'               => true,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => false,
    'show_tagcloud'              => false,
  );

  register_taxonomy('radio-category', array('radio'), $args);
}
add_action('init', 'tvcl_radio_category', 0);