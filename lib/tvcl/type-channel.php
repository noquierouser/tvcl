<?php
// Register Custom Post Type
add_action( 'init', 'tvcl_channel', 0 );
function tvcl_channel() {
  $labels = array(
    'name'                  => _x('Channels', 'Post Type General Name', 'tvcl'),
    'singular_name'         => _x('Channel', 'Post Type Singular Name', 'tvcl'),
    'menu_name'             => __('Channels', 'tvcl'),
    'name_admin_bar'        => __('Channel', 'tvcl'),
    'all_items'             => __('All channels', 'tvcl'),
    'add_new_item'          => __('Add new channel', 'tvcl'),
    'add_new'               => __('Add new', 'tvcl'),
    'new_item'              => __('New channel', 'tvcl'),
    'edit_item'             => __('Edit channel', 'tvcl'),
    'update_item'           => __('Update channel', 'tvcl'),
    'view_item'             => __('View channel', 'tvcl'),
    'search_items'          => __('Search channel', 'tvcl'),
    'not_found'             => __('No channels found', 'tvcl'),
    'not_found_in_trash'    => __('No channels in Trash', 'tvcl'),
    'featured_image'        => __('Channel logo', 'tvcl'),
    'set_featured_image'    => __('Set channel logo', 'tvcl'),
    'remove_featured_image' => __('Remove channel logo', 'tvcl'),
    'use_featured_image'    => __('Use as channel logo', 'tvcl'),
  );

  $args = array(
    'label'               => __('Channel', 'tvcl'),
    'description'         => __('Channel to be managed. Outputted to playlists.', 'tvcl'),
    'labels'              => $labels,
    'supports'            => array('title', 'thumbnail',),
    'taxonomies'          => array('channel-category'),
    'hierarchical'        => false,
    'public'              => true,
    'show_ui'             => true,
    'show_in_menu'        => true,
    'menu_position'       => 100,
    'menu_icon'           => 'dashicons-video-alt',
    'show_in_admin_bar'   => true,
    'show_in_nav_menus'   => true,
    'can_export'          => true,
    'has_archive'         => true,    
    'exclude_from_search' => true,
    'publicly_queryable'  => true,
    'capability_type'     => 'post',
    'rewrite'             => array(
      'slug'       => 'channels',
      'with_front' => false,
      'feeds'      => false
    )
  );

  register_post_type('channel', $args);
}

// Custom metaboxes
add_action('cmb2_admin_init', 'tvcl_channel_metaboxes');
function tvcl_channel_metaboxes() {
  $channel_data = new_cmb2_box(array(
    'id'           => 'channel_data',
    'title'        => __('Channel Data', 'tvcl'),
    'object_types' => array('channel'),
    'context'      => 'normal',
    'priority'     => 'high',
    'show_names'   => true
  ));

  // Channel number
  $channel_data->add_field(array(
    'id'         => 'channel_number',
    'name'       => __('Channel Number', 'tvcl'),
    'type'       => 'text',
    'column' => array(
      'position' => 2
    ),
    'attributes' => array(
      'type' => 'number',
      'min'  => 1
    )
  ));

  // URL fields
  {
    $url = $channel_data->add_field(array(
      'id'         => 'url',
      'type'       => 'group',
      'repeatable' => false,
      'options'    => array(
        'group_title' => __('Stream URLs by Quality', 'tvcl')
      )
    ));
    
    $url_hi = $channel_data->add_group_field($url, array(
      'id'   => 'hi',
      'name' => __('URL Hi', 'tvcl'),
      'desc' => __('High quality stream URL. Mostly for HD or high bandwidth streams.', 'tvcl'),
      'type' => 'textarea'
    ));

    $url_md = $channel_data->add_group_field($url, array(
      'id'   => 'md',
      'name' => __('URL Md', 'tvcl'),
      'desc' => __('Middle quality stream URL. Mostly for SD streams. If the stream has only one URL, use this field.', 'tvcl'),
      'type' => 'textarea'
    ));

    $url_lo = $channel_data->add_group_field($url, array(
      'id'   => 'lo',
      'name' => __('URL Lo', 'tvcl'),
      'desc' => __('Low quality stream URL. Mostly for streams targeted at mobiles.', 'tvcl'),
      'type' => 'textarea'
    ));
  }
}

// Channel Logo column
add_filter('manage_channel_posts_columns', 'tvcl_channel_logo_column');
function tvcl_channel_logo_column($columns) {
  $columns['logo'] = __('Channel logo', 'tvcl');

  return $columns;
}

add_action('manage_channel_posts_custom_column', 'tvcl_channel_logo_column_content', 10, 2);
function tvcl_channel_logo_column_content($column_name, $post_id) {
  if ('logo' != $column_name) {
    return;
  }

  echo the_post_thumbnail([64, 64]);
}

// Unset Pub. Date column
add_action('manage_channel_posts_columns', 'tvcl_channel_remove_columns');
function tvcl_channel_remove_columns($columns) {
  unset($columns['date']);

  return $columns;
}

// Sort by Channel Number Column
add_action('manage_edit-channel_sortable_columns', 'tvcl_channel_number_sort');
function tvcl_channel_number_sort($columns) {
  $columns['channel_number'] = 'channel_number';

  return $columns;
}

add_action('pre_get_posts', 'tvcl_channel_number_sort_query');
function tvcl_channel_number_sort_query($query) {
  if (!is_admin()) {
    return;
  }

  $orderby = $query->get('orderby');

  if ('channel_number' == $orderby) {
    $query->set('meta_key', 'channel_number');
    $query->set('orderby', 'meta_value_num');
  }
}