<?php
// Register Custom Post Type
add_action( 'init', 'tvcl_radio', 0 );
function tvcl_radio() {
  $labels = array(
    'name'                  => _x('Radios', 'Post Type General Name', 'tvcl'),
    'singular_name'         => _x('Radio', 'Post Type Singular Name', 'tvcl'),
    'menu_name'             => __('Radios', 'tvcl'),
    'name_admin_bar'        => __('Radio', 'tvcl'),
    'all_items'             => __('All radios', 'tvcl'),
    'add_new_item'          => __('Add new radio', 'tvcl'),
    'add_new'               => __('Add new', 'tvcl'),
    'new_item'              => __('New radio', 'tvcl'),
    'edit_item'             => __('Edit radio', 'tvcl'),
    'update_item'           => __('Update radio', 'tvcl'),
    'view_item'             => __('View radio', 'tvcl'),
    'search_items'          => __('Search radio', 'tvcl'),
    'not_found'             => __('No radios found', 'tvcl'),
    'not_found_in_trash'    => __('No radios in Trash', 'tvcl'),
    'featured_image'        => __('Radio logo', 'tvcl'),
    'set_featured_image'    => __('Set radio logo', 'tvcl'),
    'remove_featured_image' => __('Remove radio logo', 'tvcl'),
    'use_featured_image'    => __('Use as radio logo', 'tvcl'),
  );

  $args = array(
    'label'               => __('Radio', 'tvcl'),
    'description'         => __('Radio to be managed. Outputted to radio playlists.', 'tvcl'),
    'labels'              => $labels,
    'supports'            => array('title', 'thumbnail',),
    'taxonomies'          => array('radio-category'),
    'hierarchical'        => false,
    'public'              => true,
    'show_ui'             => true,
    'show_in_menu'        => true,
    'menu_position'       => 100,
    'menu_icon'           => 'dashicons-format-audio',
    'show_in_admin_bar'   => true,
    'show_in_nav_menus'   => true,
    'can_export'          => true,
    'has_archive'         => true,    
    'exclude_from_search' => true,
    'publicly_queryable'  => true,
    'capability_type'     => 'post',
    'rewrite'             => array(
      'slug'       => 'radios',
      'with_front' => false,
      'feeds'      => false
    )
  );

  register_post_type('radio', $args);
}

// Custom metaboxes
add_action('cmb2_admin_init', 'tvcl_radio_metaboxes');
function tvcl_radio_metaboxes() {
  $radio_data = new_cmb2_box(array(
    'id'           => 'radio_data',
    'title'        => __('Radio Data', 'tvcl'),
    'object_types' => array('radio'),
    'context'      => 'normal',
    'priority'     => 'high',
    'show_names'   => true
  ));

  // Radio number
  $radio_data->add_field(array(
    'id'         => 'radio_number',
    'name'       => __('Radio Number', 'tvcl'),
    'type'       => 'text',
    'column' => array(
      'position' => 2
    ),
    'attributes' => array(
      'type' => 'number',
      'min'  => 1
    )
  ));

  // URL field
  $url = $radio_data->add_field(array(
    'id' => 'url',
    'name' => __('Stream URL', 'tvcl'),
    'type' => 'textarea'
  ));
}

// Radio Logo column
add_filter('manage_radio_posts_columns', 'tvcl_radio_logo_column');
function tvcl_radio_logo_column($columns) {
  $columns['logo'] = __('Radio logo', 'tvcl');

  return $columns;
}

add_action('manage_radio_posts_custom_column', 'tvcl_radio_logo_column_content', 10, 2);
function tvcl_radio_logo_column_content($column_name, $post_id) {
  if ('logo' != $column_name) {
    return;
  }

  echo the_post_thumbnail([64, 64]);
}

// Unset Pub. Date column
add_action('manage_radio_posts_columns', 'tvcl_radio_remove_columns');
function tvcl_radio_remove_columns($columns) {
  unset($columns['date']);

  return $columns;
}

// Sort by Radio Number Column
add_action('manage_edit-radio_sortable_columns', 'tvcl_radio_number_sort');
function tvcl_radio_number_sort($columns) {
  $columns['radio_number'] = 'radio_number';

  return $columns;
}

add_action('pre_get_posts', 'tvcl_radio_number_sort_query');
function tvcl_radio_number_sort_query($query) {
  if (!is_admin()) {
    return;
  }

  $orderby = $query->get('orderby');

  if ('radio_number' == $orderby) {
    $query->set('meta_key', 'radio_number');
    $query->set('orderby', 'meta_value_num');
  }
}