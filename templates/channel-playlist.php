<?php
/*
Template Name: Channel Playlist
*/

/* Setting up Google Analytics tracking */
// Check if Google Analyticator is active with an uid
$ga = get_option('ga_uid') ? true : false;
// And also check if user is logged in
$ga = is_user_logged_in() ? $ga : false;

if ($ga) {
  require TVCL_PLUGIN_PATH . 'lib/ssga/SSGA.php';

  $ssga = new SSGA\SSGA(get_option('ga_uid'), 'tvcl.nqu.me');

  // Set this page for tracking
  $ssga->set_page(get_query_var('pagename'));
  $ssga->set_page_title('Channel List Fetch');
}

/* Query channels */
$channels_query = new WP_Query(array(
  'posts_per_page' => -1,
  'nopaging'       => true,
  'post_type'      => 'channel',
  'order'          => 'asc',
  'orderby'        => 'meta_value_num',
  'meta_key'       => 'channel_number'
));

/* Check which channel quality is requested */
$url_quality = isset($_GET['q']) ? $_GET['q'] : null;
$url_quality = ($url_quality == 'hi' or $url_quality == 'md' or $url_quality == 'lo') ? $url_quality : 'hi';

// GA: Track channel quality
if ($ga) {
  $ssga->set_event('ChannelList', 'Fetch', 'quality', $url_quality, true);
}

/* DEBUG: echo the plain text playlist on the browser */
if (!isset($_GET['debug'])) {
  header("Content-Type: video/vnd.mpegurl");
  header("Content-Disposition: attachment;filename=list-{$url_quality}.m3u");
} else {
  echo "<pre>";
}

if ($url_quality) :
  echo "#EXTM3U" . PHP_EOL;

  if ($channels_query->have_posts()) :
    while ($channels_query->have_posts()) :
      $channels_query->the_post();

      // Get all available qualities in the post
      $channel_qualities = get_post_meta(get_the_ID(), 'url', true)[0];

      // If the channel doesn't have the requested quality in parameters, let's gracefully degrade
      if (!isset($channel_qualities[$url_quality])) {
        switch ($url_quality) {
          case 'hi':
            $channel_url = isset($channel_qualities['md']) ? $channel_qualities['md'] : $channel_qualities['lo'];
            break;

          case 'md':
            $channel_url = isset($channel_qualities['lo']) ? $channel_qualities['lo'] : $channel_qualities['hi'];
            break;

          case 'lo':
            $channel_url = isset($channel_qualities['md']) ? $channel_qualities['md'] : $channel_qualities['hi'];
            break;
          
          default:
            $channel_url = null;
            break;
        }
      } else {
        $channel_url = get_post_meta(get_the_ID(), 'url', true)[0][$url_quality];
      }

      // If there's a channel url finally, let's make our channel object
      if ($channel_url) {
        $channel                 = new stdClass;
        $channel->name           = get_the_title();
        $channel->channel_number = get_post_meta(get_the_ID(), 'channel_number', true);
        $channel->logo           = get_the_post_thumbnail_url(get_the_ID(), 'logo_medium');
        $channel->slug           = get_post(get_the_ID(), ARRAY_A)['post_name'];
        $channel->cat_slug       = get_the_terms(get_the_ID(), 'channel-category')[0]->slug;
        $channel->cat_name       = get_the_terms(get_the_ID(), 'channel-category')[0]->name;
        $channel->url            = $channel_url;
      }

      echo sprintf(
        "#EXTINF:-1 tvg-chno=\"%s\" tvg-logo=\"%s\" tvg-id=\"cl.%s.%s\" group-title=\"%s\",%s",
        $channel->channel_number,
        $channel->logo,
        $channel->cat_slug,
        $channel->slug,
        $channel->cat_name,
        $channel->name
      );
      echo PHP_EOL;

      echo $channel->url;
      echo PHP_EOL;
    endwhile;
  else :
    _e('No channels available.', 'tvcl');
  endif;
endif;