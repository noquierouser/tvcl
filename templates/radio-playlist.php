<?php
/*
Template Name: Radio Playlist
*/

/* Setting up Google Analytics tracking */
// Check if Google Analyticator is active with an uid
$ga = get_option('ga_uid') ? true : false;
// And also check if user is logged in
$ga = is_user_logged_in() ? $ga : false;

if ($ga) {
  require TVCL_PLUGIN_PATH . 'lib/ssga/SSGA.php';

  $ssga = new SSGA\SSGA(get_option('ga_uid'), 'tvcl.nqu.me');

  // Set this page for tracking
  $ssga->set_page(get_query_var('pagename'));
  $ssga->set_page_title('Radio List Fetch');
}

/* Query channels */
$radios_query = new WP_Query(array(
  'posts_per_page' => -1,
  'nopaging'       => true,
  'post_type'      => 'radio',
  'order'          => 'asc',
  'orderby'        => 'meta_value_num',
  'meta_key'       => 'radio_number'
));

/* DEBUG: echo the plain text playlist on the browser */
if (!isset($_GET['debug'])) {
  header("Content-Type: video/vnd.mpegurl");
  header("Content-Disposition: attachment;filename=radio-list.m3u");
} else {
  echo "<pre>";
}

echo "#EXTM3U" . PHP_EOL;

if ($radios_query->have_posts()) :
  while ($radios_query->have_posts()) :
    $radios_query->the_post();

    // Get all available qualities in the post
    $radio_url = get_post_meta(get_the_ID(), 'url', true);

    // If there's a channel url finally, let's make our channel object
    if ($radio_url) {
      $radio                 = new stdClass;
      $radio->name           = get_the_title();
      $radio->channel_number = get_post_meta(get_the_ID(), 'radio_number', true);
      $radio->logo           = get_the_post_thumbnail_url(get_the_ID(), 'logo_medium');
      $radio->slug           = get_post(get_the_ID(), ARRAY_A)['post_name'];
      $radio->cat_slug       = get_the_terms(get_the_ID(), 'radio-category')[0]->slug;
      $radio->cat_name       = get_the_terms(get_the_ID(), 'radio-category')[0]->name;
      $radio->url            = $radio_url;
    }

    echo sprintf(
      "#EXTINF:-1 radio=\"true\" tvg-chno=\"%s\" tvg-logo=\"%s\" tvg-id=\"cl.%s.%s\" group-title=\"%s\",%s",
      $radio->channel_number,
      $radio->logo,
      $radio->cat_slug,
      $radio->slug,
      $radio->cat_name,
      $radio->name
    );
    echo PHP_EOL;

    echo $radio->url;
    echo PHP_EOL;
  endwhile;
else :
  _e('No radios available.', 'tvcl');
endif;