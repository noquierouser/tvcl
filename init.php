<?php
/*
Plugin Name: tvcl Manager
Plugin URI:  http://tvcl.nqu.me
Description: Extends Wordpress for managing TV and Radio channel lists for IPTV players.
Author:      noquierouser
Author URI:  http://noquierouser.com
Depends:     CMB2
License:     MIT
Version:     0.2.0
*/

// Define plugin base path
define('TVCL_PLUGIN_PATH', plugin_dir_path(__FILE__));

// TGMPA
require_once 'lib/tgmpa/class-tgm-plugin-activation.php';

// Plugin dependencies
add_action('tgmpa_register', 'tvcl_manager_plugin_dependencies');
function tvcl_manager_plugin_dependencies() {
  $plugins = array(
    array(
      'name'     => 'CMB2',
      'slug'     => 'cmb2',
      'required' => true
    ),
  );

  $config = array(
    'id'           => 'tvcl-plugin-dependencies',
    'menu'         => 'tgmpa-install-dependencies',
    'parent_slug'  => 'edit.php?post_type=channel',
    'capability'   => 'manage_options',
    'has_notices'  => true,
    'dismissable'  => false,
    'is_automatic' => true
  );

  tgmpa($plugins, $config);
}

// Plugin custom CSS
add_action('admin_enqueue_scripts', 'tvcl_manager_custom_css');
function tvcl_manager_custom_css() {
  wp_register_style('tvcl_manager_css', plugins_url('assets/wpadmin_custom.css', __FILE__), false, '0.2.0');
  wp_enqueue_style('tvcl_manager_css');
}

// Types inclusion
// Channels
include 'lib/tvcl/type-channel.php';
include 'lib/tvcl/taxonomy-channel-category.php';
// Radios
include 'lib/tvcl/type-radio.php';
include 'lib/tvcl/taxonomy-radio-category.php';

// Templates for types
include 'PageTemplater.php';

add_action('plugins_loaded', 'testTemplateIntegration');
function testTemplateIntegration() {
  $tvcl_templates = PageTemplater::get_instance();

  $tvcl_templates->add_templates([
    'templates/full-playlist.php' => 'TV & Radio Playlist',
    'templates/channel-playlist.php' => 'Channels Playlist',
    'templates/radio-playlist.php' => 'Radio Playlist'
  ]);
}